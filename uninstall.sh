#!/usr/bin/env bash
#
#    dnf-cache - uninstall.sh
#    Copyright (C) 2020  Blau Araujo <blau@debxp.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

if [[ $EUID -ne 0 ]]; then
    echo -e "\nUsage: sudo ./install.sh\n\n" 1>&2
    exit 1
fi

installer_path=$(pwd)

echo -e "\nRemoving cache database and directory..."
db_path='/var/cache/dnf-cache'
rm -rf $db_path

echo "Removing scripts..."
rm /usr/bin/dnf-cache
rm /usr/lib/dnf-cache-update

echo "Removing bash completions..."
rm /usr/share/bash-completion/completions/dnf-cache

echo -e "\nDone!\n\n"

exit
