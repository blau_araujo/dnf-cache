#!/usr/bin/env bash
#
#    dnf-cache - install.sh
#    Copyright (C) 2020  Blau Araujo <blau@debxp.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

if [[ $EUID -ne 0 ]]; then
    echo -e "\nUsage: sudo ./install.sh\n\n" 1>&2
    exit 1
fi

installer_path=$(pwd)

echo -e "\nCreating cache directory..."
db_path='/var/cache/dnf-cache'
mkdir -p $db_path

echo "Copying scripts..."
cp $installer_path/dnf-cache /usr/bin/
cp $installer_path/dnf-cache-update /usr/lib/

echo "Copying bash completions..."
cp $installer_path/completion/dnf-cache /usr/share/bash-completion/completions/

echo -e "Updating cache database...\n"
dnf-cache update

echo ""

exit
